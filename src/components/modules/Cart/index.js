import React, {Component} from 'react';
import { connect } from 'react-redux';

import Head from './Head';
import Body from './Body';
import Header from './Header';

import './style.scss';

class Cart extends Component
{
    constructor(props){
        super(props);

        this.state = {
            show:false
        }
    }

    showCart = () => {
        this.setState({
            show:!this.state.show
        })
    }

    render(){

        const {
            totalQuantity,
            totalSum,
            products
        } = this.props;

        return (
            <div id="cart">
                <Header 
                    totalQuantity   = {totalQuantity} 
                    totalSum        = {totalSum} 
                    showCart        = {this.showCart}
                />
                {  
                    this.state.show &&
                    (<div id="cart-body">
                        <table className="table table-bordered">
                            <Head/>
                            <Body products = {products} />
                        </table>
                    </div>)
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      products:      state.cart.products,
      totalQuantity: state.cart.totalQuantity,
      totalSum:      state.cart.totalSum,
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        removeProduct:(d)=>{
            dispatch({type:'REMOVE_PRODUCT', data:d})
        }
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Cart)