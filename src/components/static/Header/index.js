import React from 'react';
import Menu from '../../modules/Menu';
import Cart from '../../modules/Cart';
import './style.scss';

const Header = function(props){
    return (
        <header className="row justify-content-between align-item-center">
            <div className="col-md-7">
                <Menu />
            </div>
            <div className="col-md-5">
                <Cart />
            </div>
        </header>
    )
}


export default Header;